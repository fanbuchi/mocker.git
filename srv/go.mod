module gitee.com/fanbuchi/mocker/srv

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/jinzhu/gorm v1.9.11
	github.com/micro/go-micro v1.11.0
	github.com/nats-io/nats-server/v2 v2.1.0 // indirect
)
