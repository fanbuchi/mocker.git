package main

import (
	"gitee.com/fanbuchi/mocker/srv/datastore"
	"gitee.com/fanbuchi/mocker/srv/repository"
	"github.com/micro/go-micro/util/log"
	"github.com/micro/go-micro"
	"gitee.com/fanbuchi/mocker/srv/handler"
	"gitee.com/fanbuchi/mocker/srv/subscriber"

	mocker "gitee.com/fanbuchi/mocker/srv/proto/mocker"
)

func main() {
	session,err := datastore.CreateSession()
	session.LogMode(true)
	defer session.Close()

	session.AutoMigrate(&mocker.Mockers{})

	if err != nil {
		log.Fatalf("connection error : %v \n" , err)
	}
	repo := &repository.Mocker{DB: session}

	// New Service
	service := micro.NewService(
		micro.Name("com.xgggh.srv.mocker"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()

	// Register Handler
	//mocker.RegisterMockerHandler(service.Server(), new(handler.Mocker))
	mocker.RegisterMockerHandler(service.Server(), &handler.Mocker{Repo:repo})

	// Register Struct as Subscriber
	micro.RegisterSubscriber("com.xgggh.srv.mocker", service.Server(), new(subscriber.Mocker))

	// Register Function as Subscriber
	micro.RegisterSubscriber("com.xgggh.srv.mocker", service.Server(), subscriber.Handler)

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
