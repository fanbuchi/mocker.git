package datastore

import (
	"fmt"
	"github.com/jinzhu/gorm"
)

func CreateSession() (*gorm.DB, error) {
	host := "127.0.0.1"
	user := "root"
	dbName := "mocker"
	password := "nBFdzDj_N2_9gp2"
	return gorm.Open("mysql", fmt.Sprintf(
		"%s:%s@tcp(%s:3306)/%s?charset=utf8&parseTime=True&loc=Local",
		user, password, host, dbName,
	),
	)
}