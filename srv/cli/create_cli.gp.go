package cli

import (
	"context"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/config/cmd"
	"log"
	mocker "gitee.com/fanbuchi/mocker/srv/proto/mocker"
	"os"
)

func main(){

	cmd.Init()

	// Define our flags
	service := micro.NewService(
		micro.Flags(
			cli.Int64Flag{
				Name:  "projectId",
				Usage: "You projectId",
			},
			cli.StringFlag{
				Name:  "body",
				Usage: "Your json body",
			},
		),
	)

	// Create new greeter client
	client := mocker.NewMockerService("com.xgggh.srv.mocker", service.Client())


	// Start as service
	service.Init(
		micro.Action(func(c *cli.Context) {
			projectId := c.Int64("projectId")
			body := c.String("body")

			// Call our user service
			r, err := client.CreateMocker(context.TODO(), &mocker.Mockers{
				ProjectId:projectId,
				Body:body,
			})
			if err != nil {
				log.Fatalf("Could not create: %v", err)
			}
			log.Printf("Created: %s", r.ProjectId)
			getAll, err := client.SearchMocker(context.Background(), &mocker.Page{Page:1,Size:18})
			if err != nil {
				log.Fatalf("Could not list users: %v", err)
			}
			for _, v := range getAll.Mockers {
				log.Println(v)
			}
			os.Exit(0)
		}),
	)
	// Run the server
	if err := service.Run(); err != nil {
		log.Println(err)
	}
}
