package client

import (
	"context"

	"github.com/micro/go-micro"
	"github.com/micro/go-micro/server"
	mocker "gitee.com/fanbuchi/mocker/srv/proto/mocker"
	// mocker "path/to/service/proto/mocker"
	// cannot find module providing package path/to/service/proto/mocker
)

type mockerKey struct {}

// FromContext retrieves the client from the Context
func MockerFromContext(ctx context.Context) (mocker.MockerService, bool) {
	c, ok := ctx.Value(mockerKey{}).(mocker.MockerService)
	return c, ok
}

// Client returns a wrapper for the MockerClient
func MockerWrapper(service micro.Service) server.HandlerWrapper {
	client := mocker.NewMockerService("com.xgggh.srv.mocker", service.Client())

	return func(fn server.HandlerFunc) server.HandlerFunc {
		return func(ctx context.Context, req server.Request, rsp interface{}) error {
			ctx = context.WithValue(ctx, mockerKey{}, client)
			return fn(ctx, req, rsp)
		}
	}
}
