package main

import (
	"github.com/micro/go-micro/util/log"

	"github.com/micro/go-micro"
	"gitee.com/fanbuchi/mocker/api/handler"
	"gitee.com/fanbuchi/mocker/api/client"

	mocker "gitee.com/fanbuchi/mocker/api/proto/mocker"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("com.xgggh.api.mocker"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init(
		// create wrap for the Mocker srv client
		micro.WrapHandler(client.MockerWrapper(service)),
	)

	// Register Handler
	mocker.RegisterMockerHandler(service.Server(), new(handler.Mocker))

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
