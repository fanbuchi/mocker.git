module gitee.com/fanbuchi/mocker/api

go 1.13

require (
	gitee.com/fanbuchi/mocker/srv v0.0.0
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.11.0
	mockerSrv v0.0.0 // indirect
)

replace mockerSrv => ../srv

replace gitee.com/fanbuchi/mocker/srv => ../srv
