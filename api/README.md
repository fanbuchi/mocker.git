# Mocker Service

This is the Mocker service

Generated with

```
micro new gitee.com/fanbuchi/mocker/api --namespace=com.xgggh --alias=mocker --type=api
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: com.xgggh.api.mocker
- Type: api
- Alias: mocker

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev
```

## Usage

A Makefile is included for convenience

Build the binary

```
make build
```

Run the service
```
./mocker-api
```

Build a docker image
```
make docker
```