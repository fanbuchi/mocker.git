package handler

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
	"github.com/micro/go-micro/util/log"
	"github.com/micro/go-micro/client"
	mocker "gitee.com/fanbuchi/mocker/srv/proto/mocker"
)

func MockerCall(w http.ResponseWriter, r *http.Request) {
	// decode the incoming request as json
	log.Log("web mocker call")
	var request map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// call the backend service
	mockerClient := mocker.NewMockerService("com.xgggh.srv.mocker", client.DefaultClient)
	rsp, err := mockerClient.Call(context.TODO(), &mocker.Request{
		Name: request["name"].(string),
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// we want to augment the response
	response := map[string]interface{}{
		"msg": rsp.Msg,
		"ref": time.Now().UnixNano(),
	}

	// encode and write the response as json
	if err := json.NewEncoder(w).Encode(response); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}
