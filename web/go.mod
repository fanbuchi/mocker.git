module gitee.com/fanbuchi/mocker/web

go 1.13

require (
	gitee.com/fanbuchi/mocker/srv v0.0.0
	github.com/go-log/log v0.1.0
	github.com/micro/go-micro v1.11.0
)

replace gitee.com/fanbuchi/mocker/srv => ../srv
