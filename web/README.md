# Mocker Service

This is the Mocker service

Generated with

```
micro new gitee.com/fanbuchi/mocker/web --namespace=com.xgggh --alias=mocker --type=web
```

## Getting Started

- [Configuration](#configuration)
- [Dependencies](#dependencies)
- [Usage](#usage)

## Configuration

- FQDN: com.xgggh.web.mocker
- Type: web
- Alias: mocker

## Dependencies

Micro services depend on service discovery. The default is multicast DNS, a zeroconf system.

In the event you need a resilient multi-host setup we recommend consul.

```
# install consul
brew install consul

# run consul
consul agent -dev

go run main.go
http://localhost:63430 ok index.html
http://localhost:8080 fail 404
micro api --handler=http --namespace=com.xgggh.web
http://localhost:8080/mocker/call ok
{"name":"zz"}
```