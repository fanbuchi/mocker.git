package main

import (
        "gitee.com/fanbuchi/mocker/web/handler"
        "github.com/micro/go-micro/util/log"
        "github.com/micro/go-micro/web"
        "net/http"
)

func main() {
	// create new web service
        service := web.NewService(
                web.Name("com.xgggh.web.mocker"),
                web.Version("latest"),
                //web.StaticDir("html"),
        )

	// initialise service
        if err := service.Init(); err != nil {
                log.Fatal(err)
        }

	// register html handler
	service.Handle("/", http.FileServer(http.Dir("html")))

	// register call handler
	service.HandleFunc("/mocker/call", handler.MockerCall)

	// run service
        if err := service.Run(); err != nil {
                log.Fatal(err)
        }
}
