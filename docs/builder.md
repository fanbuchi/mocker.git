```
micro GO111MODULE=on
go env -w GOPROXY=https://mirrors.aliyun.com/goproxy,direct
```

```
work dir
protoc --proto_path=import_proto_path:. --go_out=. --micro_out=. proto/api.proto
protoc --proto_path=. --go_out=. --micro_out=. proto/mocker/mocker.proto
protoc --proto_path=D:/gopath/src --go_out=. --micro_out=. D:/gopath/src/gitee.com/fanbuchi/mocker/srv/proto/mocker/mocker.proto
cd src
protoc --proto_path=D:/gopath/src --go_out=. --micro_out=. D:/gopath/src/gitee.com/fanbuchi/mocker/api/proto/mocker/mocker.proto
```

```
micro GO111MODULE=on go env -w GOPROXY=https://mirrors.aliyun.com/goproxy,direct

work dir 
protoc --proto_path=. --go_out=. --micro_out=. proto/mocker/mocker.proto 
cd src 
D:\gopath\src>protoc --proto_path=D:/gopath/src --go_out=. --micro_out=. D:/gopath/src/gitee.com/fanbuchi/mocker/api/proto/mocker/mocker.proto

#####定制protoc tag

github.com/golang/protobuf/protoc-gen-go/genertor.go tag := fmt.Sprintf("protobuf:%s json:%q orm:%s", g.goTag(message, field, wiretype), jsonName+",omitempty", jsonName)

micro api  --handler=rpc --namespace=com.xgggh.srv
go run main.go
micro call com.xgggh.srv.mocker Mocker.Call "{\"name\":\"zhangsan\"}"
POST: 
http://localhost:8080/mocker/mocker/call ok
http://localhost:8080/mocker/call ok
{
    "name": "zz"
}
RPC:
micro api  --handler=rpc --namespace=go.micro.srv --enable_rpc=true
http://localhost:8080/rpc?service=com.xgggh.srv.mocker&method=Mocker.Call&request={"name":"zzz"}

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/go-micro v1.11.0
	mocker v0.0.0
)

replace mocker => ../srv
micro api  --handler=api --namespace=go.micro.api
micro call com.xgggh.api.mocker Mocker.Call "{}"
http://localhost:8080/mocker/call ok
x-www-form-urlencoded
name = zzz

```

